<?php
	//this file is to store all basic functions
	
	function mysql_prep($value){
		$magic_quotes_active = get_magic_quotes_gpc();
		//i.e. php >= 4.3.0
		$new_enough_php = function_exists("mysql_real_escape_string");
		
		if (new_enough_php){
			if(magic_quotes_active){ $value = stripcslashes($value); }
			$value = mysql_real_escape_string($value);
			} else {//before php 4.3.0
				if(!$magic_quotes_active) {$value = addslashes($value);}
			}
			return $value;
		}
	
	function mysql_add_query($query, $connection2){
		global $connection;
		if (mysql_query($query, $connection)){
			return 0;
		}
		else{
			return "MySQL ERROR: ". mysql_error();
		}
	}
	
	function mysql_get_data_by_name($table, $name, $selection, $connection){
		$aResults=array();
		$query = "SELECT {$selection} FROM `{$table}` WHERE name = '{$name}'";
		$results = mysql_query($query, $connection);
		while($row = mysql_fetch_array($results)){
			$aResults[] = $row[$selection];
		}
		return $aResults;
	}
	
	function redirect_to($location = NULL){
		if($location != NULL){
			header("Location: {$location}");
			exit;
		}
	}
	
	function confirm_query($result_set){
		if (!$result_set){
				die("Database query failed: " . mysql_error());
				}
		}
		
	function get_maincategorys(){
		global $connection;
		$query = "SELECT *
				FROM category
				ORDER BY position ASC" ;
		$menu_set = mysql_query($query, $connection);
		confirm_query($menu_set);
		return $menu_set;
		}
		
	function get_maincategorysPublic(){
		global $connection;
		$query = "SELECT *
				FROM category
				WHERE visable != '1'
				ORDER BY position ASC" ;
		$menu_set = mysql_query($query, $connection);
		confirm_query($menu_set);
		return $menu_set;
		}
		
	function get_itemslist(){
		global $connection;
		$query = "SELECT *
				FROM items
				ORDER BY id ASC";
		$itemlist_set = mysql_query($query, $connection);
		confirm_query($itemlist_set);
		return $itemlist_set;
	}
	
	function get_itemslistPublic(){
		global $connection;
		$query = "SELECT *
				FROM items
				WHERE visable != '1'
				ORDER BY id ASC";
		$itemlist_set = mysql_query($query, $connection);
		confirm_query($itemlist_set);
		return $itemlist_set;
	}
	
	function get_id_from_name($categoryname, $table){
		global $connection;
		$query = "SELECT id
				FROM `{$table}`
				WHERE name = '{$categoryname}'";
		$returnlist = mysql_query($query, $connection);
		confirm_query($returnlist);
		while($row = mysql_fetch_array($returnlist)){
			$aResults[] = $row['id'];
		}
		return $aResults[0];
	}
	
	function get_position_from_id($categoryid, $table){
		global $connection;
		$query = "SELECT position
					FROM `{$table}`
					WHERE id = '{$categoryid}'";
		$results = mysql_query($query, $connection);
		confirm_query($results);
		while($row = mysql_fetch_array($results)){
			$aResults[] = $row['position'];
		}
		return $aResults[0];
		
	}
	
	function delete_by_id($category_id, $table){
		global $connection;
		if($table=='category'){$id = 'id';}else{$id = 'category_id';}
		$query = "DELETE FROM {$table}
				WHERE {$id} = {$category_id}";
		if (mysql_query($query, $connection)){
			return 0;
		}
		else{
			return "MySQL ERROR: ". mysql_error();
		}
		return $query;
	}
	
	function json2arr($json){
		//change the ,} to just }
		$json = str_replace(',}','}',$json);
		//turn the JSON string to a PHP Array
    	$data = json_decode($json, true);
    	return $data;
	}

	function set_position($position, $categoryname, $add_del_alt){
		global $connection;
		$results = '';
		$action = null;
		$tempposition = null;
		$categorylist = get_maincategorys();
		while ($categoryitem = mysql_fetch_array($categorylist)){
			switch($add_del_alt){
				case 'add':
					if (($categoryitem['position'] == $position ||
						$categoryitem['position'] == $tempposition))
					{$action = ($categoryitem['position'] + 1); $dosomething = TRUE;}
					else{$dosomething = FALSE;}
					break;
				case 'del':
					if(($categoryitem['position'] == ($position+1) ||
						$categoryitem['position'] == ($count)))
					{$action = ($categoryitem['position'] - 1); $dosomething = TRUE;}
					else{$dosomething = FALSE;}
					break;
				default:
					$dosomething = FALSE;
					break;
			}
			
			if($categoryname == $categoryitem['name']){$dosomething = False;}
			
			if ($dosomething == TRUE){
				$tempposition = $action;
				$count = $categoryitem['position'] + 1;
				$tempid = $categoryitem['id'];
			
				$tempquery = "UPDATE category
						SET position = {$tempposition}
						WHERE id = {$tempid}";
				if (mysql_query($tempquery, $connection)){
				}
				else{
					$results += " Postion Error: ". mysql_error();
				}
			}
		}
		if ($results== ''){$results = 0;}
		return $results;
	}
	
	function change_position($oldpos, $newpos, $categoryname){
		global $connection;
		$action = null;
		$dosomething = FALSE;
		$results = null;
		$tempposition = null;
		$count = $oldpos;
		//Checks if the position changed and if so if its a ++ or --
		if($oldpos==$newpos){$action = 0; $results = 0;}
		else if($oldpos<$newpos){$action = 'up';}else{$action = 'down';}
		//If change then query db and make changes
		if(!$action == 0){
			$categorylist = get_maincategorys();
			while($items = mysql_fetch_array($categorylist)){
				//If action is down
				if(($items['position'] == $newpos ||
						$items['position'] == $tempposition
						&& $action == 'down'))
					{$dothis = ($items['position'] + 1); 
					 $dosomething = TRUE;}
				//If action is up
				else if($items['position']>=$oldpos
					 && $items['position']<=$newpos 
					 && $action == 'up')
					{$dothis = ($items['position'] - 1);
					 $dosomething = TRUE;}
				//none of the above
				else{$dosomething = FALSE;}
				
				if($categoryname == $items['name']){$dosomething = FALSE;}
				
				//If everything is set update itemrow with new pos
				if($dosomething == TRUE)
				{
					$tempposition = $action == 'up'? ($items['position'] - 1):($items['position'] + 1);
					$count = $items['position'];
					$tempid = $items['id'];
				
					$tempquery = "UPDATE category
							SET position = {$tempposition}
							WHERE id = {$tempid}";
					if (mysql_query($tempquery, $connection)){
					}
					else{
						$results += " Postion Error: ". mysql_error();
					}
				}
				
			}
		}
		if ($results== null){$results = 0;}
		return $results;
	}

?>
