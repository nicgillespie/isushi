<?php require_once('browser.php'); ?> <!-- Adds class to test and see if brower is mobile or not -->
<!DOCTYPE html>
<!--<html manifest="application.manifest">-->
<html>
<head>
<?PHP 
	$browser = new Browser();
	$browser = $browser->getBrowser();
	if($browser == Browser::BROWSER_BLACKBERRY || $browser == Browser::PLATFORM_ANDROID || $browser == Browser::BROWSER_IPHONE || $browser == Browser::BROWSER_IPOD || $browser == Browser::BROWSER_POCKET_IE || $browser == Browser::PLATFORM_IPAD || $browser == Browser::BROWSER_FIREFOX_MOBILE){
	//IS MOBILE
	include('mobile.php');
	}else{
	//IS DESKTOP
	include('desktop.php');
	}
?>
<!-- [if IE]>
<script type='text/javascript'>
	alert("We are sorry but your web brower is compatable with this site. Please download a more up to date browser.");
	window.location = "http://chrome.google.com";
</script>
<![endif]-->
<!--icon -->
<link rel="icon" href="Sushi.png"/>
<link rel="apple-touch-icon" href="ios-icon.png"/>
<!--encoding -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Style Sheet Classes -->
<link rel="stylesheet" type="text/css" href="./css/fonts.css"/>
<link rel="stylesheet" type='text/css' href='./css/emailform.css' />
<!--Javascript files -->
<script type="text/javascript" src="./js/nav.js"></script>
<!--<script type="text/javascript" src="./js/emailform.js"></script>-->
<!--UPDATE THE APP CACHE IF NEEDED -->
<!--<script type="text/javascript" src='./js/appCache.js'></script>-->
<!--DETECT IF THE USER IS ON A PHONE OR DESKTOP-->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<title>iSushi Express</title>
<script type='text/javascript'>
 	if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod' || navigator.platform.indexOf('android') >= 0)
	{
		window.location = "http://prettyodd.dyndns-ip.com/mobile.html";
	}
</script>

</head>
<body>
	<!-- email form for dropdown -->
	<div class='fullform'>
		<p>Please fill out this form for email alerts about special offers!</p>
		<div id="emailblock">email:&nbsp; <input class="emailfield" id="emailfield"/></div>
		<div id="firstnameblock">first name:&nbsp; <input id="firstname"></div>
		<div id="lastnameblock">last name:&nbsp; <input id="lastname"/></div>
		<button id='submitbtn'>Submit</button>
		<button id='cnlbtn'>Cancel</button>
		<p id="validationerror">Please fix the field(s) in red</p>
	</div>

	<!-- email form -->
	<div class="emailform">
		<p>Sign up for email specials!</p>
		email: <input class="emailfield" id='emailfieldsmall' />
		<button class="emailbtn"></button>
	</div>
	<!-- logo -->
	<div class="logo" id="logo"></div>

	<hr class="green" /> 
	<nav id='navelement'>
		<!-- Main page nav -->
	<div class="mainnav">
		<ul id='navbar'>
		<li id='home'>home</li>
		<li id='info'>our info</li>
		<li id='menu'>menu</li>
		<li id='order'>order online</li>
		</ul>
	</div>
	
	<!-- Facebook, Twitter, Google+ icons -->
	<div class="followus">
		<a class="gpp" href="https://plus.google.com/112805263410268252510"></a>
		<a class="tw" href="http://www.twitter.com/isushicafe/"></a>
		<a class="fb" href="http://www.facebook.com/pages/ISushi-Express/338453752835528"></a>
	</div>
	</nav>

	
	<hr class="red" /> 
