<?php
	require('dbconnection.php');
	require('fuctions.php');
	//set up arrays
	$category = array();
	$categorydisc = array();
	$subcategory = array();
	$itemname = array();
	$itemdisc = array();
	$price = array();
	$price2 = array();
	$pricetitle = array();
	$pricetitle2 = array();
	//query db
	$maincategorys = get_maincategorysPublic();
	while ($menudata = mysql_fetch_array($maincategorys)){
		//put db fields into resp array
		$category[$menudata['id']] = $menudata['category'];
		$categorydisc[$menudata['id']] = $menudata['categorydisc'];
		$subcategory[$menudata['id']] = $menudata['subcategory'];
		$itemname[$menudata['id']] = $menudata['itemname'];
		$itemdisc[$menudata['id']] = $menudata['discription'];
		$price[$menudata['id']] = $menudata['price'];
		$price2[$menudata['id']] = $menudata['price2'];
		$pricetitle[$menudata['id']] = $menudata['pricetitle'];
		$pricetitle2[$menudata['id']] = $menudata['price2title'];
	}
	//count number of total rows
	$rows = count($category);
	//trim down the category and its disc of duplicates
	$category = array_unique($category);
	$categorydisc = array_unique($categorydisc);
	//Create menudb
	$menudb = array();
	//Add each category as a key to the array with another array as the value
	foreach ($category as $key){
		$menudb[$key] = array();
	}
	//Sets up the multidemintonal array inside the menudb
	for ($i = 1; $i <= $rows; $i++){
		if (isset($category[$i]))
		{
			//If category has dis then add it, if not put a space
			if (isset($categorydisc[$i])){
			$menudb[$category[$i]]['categorydisc'] = $categorydisc[$i];
			} else {$menudb[$category[$i]]['categorydisc'] = "";}
			//sets up the other arrays inside of menudb
			$menudb[$category[$i]]['subcategory'] = array();
			$menudb[$category[$i]]['itemname'] = array();
			$menudb[$category[$i]]['itemdisc'] = array();
			$menudb[$category[$i]]['price'] = array();
			$menudb[$category[$i]]['price2'] = array();
			$menudb[$category[$i]]['pricetitle'] = array();
			$menudb[$category[$i]]['pricetitle2'] = array();
		}
	}
	//Finds what Category item it belongs to and adds it
	$arrKey = array_keys($category);
	for ($i = 1; $i <= $rows; $i++){
		$cate = null;
		switch (true){
			case ($i < $arrKey[1]):
				$cate = $category[$arrKey[0]];
				break;
			case ($i >= $arrKey[1] && $i < $arrKey[2]):
				$cate = $category[$arrKey[1]];
				break;
			case ($i >= $arrKey[2] && $i < $arrKey[3]):
				$cate = $category[$arrKey[2]];
				break;
			case ($i >= $arrKey[3] && $i < $arrKey[4]):
				$cate = $category[$arrKey[3]];
				break;
			case ($i >= $arrKey[4] && $i < $arrKey[5]):
				$cate = $category[$arrKey[4]];
				break;
			case ($i >= $arrKey[5] && $i < $arrKey[6]):
				$cate = $category[$arrKey[5]];
				break;
			case ($i >= $arrKey[6] && $i < $arrKey[7]):
				$cate = $category[$arrKey[6]];
				break;
			case ($i >= $arrKey[7] && $i < $arrKey[8]):
				$cate = $category[$arrKey[7]];
				break;
			case ($i >= $arrKey[8] && $i < $arrKey[9]):
				$cate = $category[$arrKey[8]];
				break;
			case ($i >= $arrKey[9] && $i < $arrKey[10]):
				$cate = $category[$arrKey[9]];
				break;
			case ($i >= $arrKey[10] && $i < $arrKey[11]):
				$cate = $category[$arrKey[10]];
				break;
			case ($i >= $arrKey[11] && $i < $arrKey[12]):
				$cate = $category[$arrKey[11]];
				break;
			case ($i >= $arrKey[12] && $i < $arrKey[13]):
				$cate = $category[$arrKey[12]];
				break;
			case ($i >= $arrKey[13] && $i < $arrKey[14]):
				$cate = $category[$arrKey[13]];
				break;
			case ($i >= $arrKey[14] && $i < $arrKey[15]):
				$cate = $category[$arrKey[14]];
				break;
			case ($i >= $arrKey[15] && $i < $arrKey[16]):
				$cate = $category[$arrKey[15]];
				break;
			case ($i >= $arrKey[16]):
				$cate = $category[$arrKey[16]];
				break;
		}
		$menudb[$cate]['subcategory'][$i] = $subcategory[$i];
		$menudb[$cate]['itemname'][$i] = $itemname[$i];
		$menudb[$cate]['itemdisc'][$i] = $itemdisc[$i];
		$menudb[$cate]['price'][$i] = $price[$i];
		$menudb[$cate]['price2'][$i] = $price2[$i];
		$menudb[$cate]['pricetitle'][$i] = $pricetitle[$i];
		$menudb[$cate]['pricetitle2'][$i] = $pricetitle2[$i];
	}
	
	echo json_encode($menudb);
	//TESTING CODE
	//print_r($menudb,false);
	//print_r($category,false);
	//print_r(count(array_keys($category),false));
	//print_r(array_keys($category),false);
	//mysql_close($connection);
?>
