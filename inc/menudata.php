<?php
	header('Access-Control-Allow-Origin: *');
	require('dbconnection.php');
	require('fuctions.php');
	//set up arrays
	$categoryid = array();
	$category = array();
	$categorydisc = array();
	$categorypos = array();
	$categoryvis = array();
	$itemname = array();
	$itemdisc = array();
	$price = array();
	$price2 = array();
	$pricetitle = array();
	$pricetitle2 = array();
	$category_id = array();
	$item_id = array();
	//query db
	$maincategorys = get_maincategorys();
	$tempx = 1;
	while ($menudata = mysql_fetch_array($maincategorys)){
		//put db fields into resp array
		$categoryid[$tempx] = $menudata['id'];
		$category[$tempx] = $menudata['name'];
		$categorydisc[$tempx] = $menudata['disc'];
		$categorypos[$tempx] = $menudata['position'];
		$categoryvis[$tempx] = $menudata['visable'];
		$tempx++;
	}
	$itemlist = get_itemslist();
	$tempx = 1;
	while($itemdata = mysql_fetch_array($itemlist)){
		$item_id[$tempx] = $itemdata['id'];
		$itemname[$tempx] = $itemdata['itemname'];
		$itemdisc[$tempx] = $itemdata['discription'];
		$price[$tempx] = $itemdata['price'];
		$price2[$tempx] = $itemdata['price2'];
		$pricetitle[$tempx] = $itemdata['pricetitle'];
		$pricetitle2[$tempx] = $itemdata['price2title'];
		$category_id[$tempx] = $itemdata['category_id'];
		$tempx++;
	}
	mysql_close($connection);
	//count number of total rows
	$items_count = count($itemname);
	$category_count = count($category);
	
	//Create menudb
	$menudb = array();
	//Add each category as a key to the array with another array as the value
	foreach ($category as $key){
		$menudb[$key] = array();
	}
	//Sets up the multidemintonal array inside the menudb
	for ($i = 1; $i <= $category_count; $i++){
		if (isset($category[$i]))
		{
			//If category has dis then add it, if not put a space
			if (isset($categorydisc[$i])){
			$menudb[$category[$i]]['categorydisc'] = $categorydisc[$i];
			} else {$menudb[$category[$i]]['categorydisc'] = "";}
			if (isset($categoryvis[$i])){
			$menudb[$category[$i]]['categoryvis'] = $categoryvis[$i];
			} else {$menudb[$category[$i]]['categoryvis'] = 0;}
			$menudb[$category[$i]]['categorypos'] = $categorypos[$i];
			//sets up the other arrays inside of menudb
			$menudb[$category[$i]]['itemname'] = array();
			$menudb[$category[$i]]['itemdisc'] = array();
			$menudb[$category[$i]]['price'] = array();
			$menudb[$category[$i]]['price2'] = array();
			$menudb[$category[$i]]['pricetitle'] = array();
			$menudb[$category[$i]]['pricetitle2'] = array();
			$menudb[$category[$i]]['category_id'] = array();
		}
	}
	//Finds what Category item it belongs to and adds it
	$arrKey = array_keys($category);
	for($i=1; $i <= $items_count; $i++){
		foreach($arrKey as $key){
			if($categoryid[$key] == $category_id[$i]){
				$cate = $category[$key];
				$menudb[$cate]['item_id'][$i] = $item_id[$i];
				$menudb[$cate]['itemname'][$i] = $itemname[$i];
				$menudb[$cate]['itemdisc'][$i] = $itemdisc[$i];
				$menudb[$cate]['price'][$i] = $price[$i];
				$menudb[$cate]['price2'][$i] = $price2[$i];
				$menudb[$cate]['pricetitle'][$i] = $pricetitle[$i];
				$menudb[$cate]['pricetitle2'][$i] = $pricetitle2[$i];
				$menudb[$cate]['category_id'][$i] = $category_id[$i];
				if($i == $items_count){
				$categoryid_max = $category_id[$i];
				}
			}
		}
	}
	$menudb['category_id_max'] =$categoryid_max;
	echo json_encode($menudb);
	//print_r($categoryid_max);
	//TESTING CODE
	//print_r($menudb,false);
	//print_r($category,false);
	//print_r(count(array_keys($category),false));
	//print_r(array_keys($category),false);
	//var_dump($categoryid);
	//var_dump(array_keys($category_id));
?>
