// Scroll down for full email form
//jQuery Starts
$(document).ready(function(){

//Enterkey Pressed while in the .emailformsmall feild.
$("#emailfieldsmall").keyup(function(){
	if(event.keyCode == 13){
		$(".emailbtn").click();
		$("#emailfield").focus();
	}
});

//emailbtn 
$(".emailbtn").click(function(){
	//copys email address to main form
	document.getElementById('emailfield').value = document.getElementById('emailfieldsmall').value;    
	$(".fullform").slideDown("slow");
  });
//cnl btn pressed
$("#cnlbtn").click(function(){
    $(".fullform").slideUp("slow");
  });
//submit btn pressed
$("#submitbtn").click(function(){
	emailgood = validateEmail();
	firstnamegood = validatename('firstname','firstnameblock');
	lastnamegood = validatename('lastname','lastnameblock');
	if (emailgood == true && firstnamegood == true && lastnamegood == true){
		document.getElementById('validationerror').style.display="none";
		var email=document.getElementById('emailfield').value;
		var firstname=document.getElementById('firstname').value;
		var lastname=document.getElementById('lastname').value;
		var url=(document.URL);
		
	}
});

//if ENTER is pressed whil in full email form
$("#emailfield").keyup(function(){
	if(event.keyCode == 13){
		$("#submitbtn").click();
	}
});
$("#firstname").keyup(function(){
	if(event.keyCode == 13){
		$("#submitbtn").click();
	}
});
$("#lastname").keyup(function(){
	if(event.keyCode == 13){
		$("#submitbtn").click();
	}
});
//if tab is hit on cnl reset focus to email
$("#cnlbtn").keydown(function(){
	if(event.keyCode == 9){
		$("#emailfield").focus();
	}
});
/*jQueryEnds */});

//email validation
function validateEmail()
{
var email=document.getElementById('emailfield').value;
var atpos=email.indexOf("@");
var dotpos=email.lastIndexOf(".");
var emailgood = false;
var iChars = "!#$%^&*()+=-[]\\\';,/{}|\":<>?";
for (var i = 0; i < email.length; i++) {
  	if (iChars.indexOf(email.charAt(i)) != -1) {
  	document.getElementById('emailblock').style.color="#ff3c3c";
	document.getElementById('validationerror').style.display="inline";
  	return false;
  	}
  }
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length || email.length > 45)
  {
  document.getElementById('emailblock').style.color="#ff3c3c";
	document.getElementById('validationerror').style.display="inline";
  return false;
  }else{
	document.getElementById('emailblock').style.color="white"; 
	emailgood = true;
	return emailgood;
	}
}

//name validation
function validatename(namefield,labeldiv)
{
var tempname=document.getElementById(namefield).value;
var namegood = false;
var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?1234567890";
for (var i = 0; i < tempname.length; i++) {
  	if (iChars.indexOf(tempname.charAt(i)) != -1) {
  	document.getElementById(labeldiv).style.color="#ff3c3c";
	document.getElementById('validationerror').style.display="inline";
  	return false;
  	}
  }
if (tempname.length < 2 || tempname.length > 45){
	document.getElementById(labeldiv).style.color="#ff3c3c";
	document.getElementById('validationerror').style.display="inline";
  	return false;
	}else{
	document.getElementById(labeldiv).style.color="white";
	namegood = true;
	return namegood;
	}
}

//AJAX for email upload and verification
function submitdata(temail, tfistname, tlastname, turl){
$(document).ready(function(){
	$.post("./inc/submitemail.php", {temail, tfirstname, tlastname, turl}, function(data){
	alert("Data Loaded: " + data);
	});
});
}
