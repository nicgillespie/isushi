$(document).ready(function(){
	sitetitle();
	var at = 0;
//	sets height of slideshow
	var linkscate = $('#linkscategory');
	var linkscateT = linkscate.position().top;
	var linksHeight = $('#linkscategory').height();
	changeHeight = linkscateT+linksHeight;
	$('#pagebody').css('top', changeHeight);
	//	If the menubtn is pressed
	$('.links').click(function(){
		if (at == 0){
			var menuwidth = $('#menunav').width();
			$('#linkscategory').animate({width: menuwidth+'px'},700);
			$('#linksstart').css('display', 'block');
			setTimeout("$('#linksstart').animate({opacity: 1.0}, 1400)",400);
			at = 1;
		}else if (at == 1){
			setTimeout("$('#linkscategory').animate({width:'0'},700)",500);
			$('#linksstart').animate({opacity: 0.0},700);
			setTimeout("$('#linksstart').css('display', 'none')", 1400);
			at = 0;
		}
	});

	$(window).resize(function(){
	//	Run if orentation of device changes
		if (window.orientation == 0){
			var ori = 'portrait';
		}else{
			var ori = 'landscape';
		}
		ort(ori);
		$(window).bind("orientationchange", function(event){
			ort(event.orientation);
		});
	});

	setTimeout("$('#pagebody').css('visibility','visible')","500");
});


function sitetitle(){
	var theight = $('#menunav').height(); 
	var fsize = theight*.6;
	var field = $('#sitename');
//	Sets new font size
	field.css('font-size',fsize+'px');
	
// Find and set new margin
	var fheight = field.height(); 
	var tmargin = ((theight-fheight)*.5);
	field.css('margin-top', tmargin);

}

//Run if orentation changes
//shows or hides nav bars and sets mainpage to fill screen
function ort(ori){
	var body = $('#pagebody');
	if (ori == 'portrait'){
		$('#menunav').css('display', 'block');
		body.css('top', changeHeight);
		//body.css('height', '550px');
	}else if (ori == 'landscape'){
		$('#menunav').css('display', 'none');
		body.css('top', '0');
		//body.css('height', '100%');
	}
}
