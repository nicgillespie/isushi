//Counts Code Validation Issues
var error = 0;

//When a field is in focus its clears the text if field has default text in it
function clearfield(fieldid){
	var value = $('#'+fieldid).val();
	if (value == 'Name*' || value == 'Phone Number' || value == 'Email*' || value == 'Comment, Question, or Concerns*'){
		$('#'+fieldid).val('');
		if ($('#'+fieldid).css('color') == "rgb(255, 0, 0)"){
			$('#'+fieldid).css('color', '#444');
			error = error-1;
		}
	}
}

//Test fields after they lose focus 
function resetfield(fieldid){
	var field = $('#'+fieldid)
	var value = field.val();

	if ($('#'+fieldid).val() == ''){
		switch (fieldid){
			case 'name':
				field.val('Name*');
				field.css('color', 'Red');
				error = error+1;
				break;
			case 'number':
				$('#'+fieldid).val('Phone Number');
				break;
			case 'email':
				$('#'+fieldid).val('Email*');
				field.css('color', 'Red');
				error = error+1;
				break;
			case 'comment':
				$('#'+fieldid).val('Comment, Question, or Concerns*');
				field.css('color', 'Red');
				error = error+1;
				break;
		}
	}
}
//On Enter Key Pressed
function onKeyDownSubmit(event){
	if (event.keyCode == 13){
		formSubmit();
	}
}

//Submit forms to server
function formSubmit(){
	var resultsfield = $('#results');
	if (error != 0){
	//Errors on page!
	}else{
	//Form submits with no errors!
		var nameField = $('#name').val();
//		nameField = nameField.serialize();
		var phoneField = $('#number').val();
//		phoneField = phoneField.serialize();
		var emailField = $('#email').val();
//		emailField = emailField.serialize();
		var commentField = $('#comment').val();
//		commentField = commentField.serialize();
		var http;
		//init AJAX request
		http = new XMLHttpRequest();
		//Check to see if Ajax was successful
		http.onreadystatechange = function(){
			if (http.readyState==4 && http.status==200){
				//Ajax was successful
				resultsfield.text(http.responseText);
			}
		}
		//Make GET Ajax request
		http.open("GET",'/inc/contact.php?name='+nameField+'&number='+phoneField+'&email='+emailField+'&comment='+commentField, true);
		//Send Request
		resultsfield.text('Sending...');
		http.send();
	}
}
