$(document).ready(function(){
	//Run menusize when doc loads and is rendered
		menusize();
		menuitemsWidth();
	//Run menusize when window resized
	$(window).resize(function(){
		menusize();
		menuitemsWidth();
	});
	
});

//*****************************
//FIND HEIGHT AND WIDTH NEEDED*
//*****************************
//Finds the loc and height of the red hr bar and sub. from the total height of the view port
//and sets value to menu id's 
function menusize(){
	//finds height needed
	var hrbar = $(".red");
	var hrheight = hrbar.outerHeight(true);
	var top = (hrbar.position()).top;
	var page = $(window);
	var height = page.height();
	var menuitem = $("#menutitle");
	menuitem = menuitem.outerHeight(true);
	var newheight = height-(top+hrheight+menuitem);
	//applies new height
	$("#menuarea").height(newheight);
	$("#menuitems").height(newheight);
	$("#menucategory").height(newheight);
}
//Find the width of the windows and the width+margin to menucategory and sets the new width to
//#menuitems
function menuitemsWidth() {
	//finds width needed
	var category = $('#menucategory').outerWidth(true);
	var menuarea = $('#menuarea').width();
	var newWidth = menuarea-category;
	//apply new width
	$('#menuitems').width(newWidth);
}
