$(document).ready(function(){
	//Run functions on window ready
	menutitle();
	menubtn();
	$(window).resize(function(){
	//	Run if orentation of device changes
		if (window.orientation == 0){
			var ori = 'portrait';
		}else{
			var ori = 'landscape';
		}
		ortchn(ori);
		$(window).bind("orientationchange", function(event){
			ortchn(event.orientation);
			menutitle();
		});
	
	});
	$('#categoryname').html('Please pick a category');
//	If the menubtn is pressed
	$('.menubtn').click(function(){
		$('#menucategory').slideToggle('slow');
	});
	$('#menucategory').click(function(){
		$('#menucategory').slideUp('slow');
		menutitle();
	});
});

function menuinfo(){/*command removed from top line as per request*/
	$('#menutitle').html('^ Click the Sushi above for more!');
	setTimeout("$('#menutitle').slideDown('slow')",1000);
	setTimeout("$('#menutitle').slideUp('slow')",4000);
}

//For header toolbar
function menubtn(){
	var newbtn = '<div class="menubtn"></div>'
	$('#categoryname').before(newbtn);
	

}

function menutitle(){
	var theight = $('#menunav').height(); 
	var fsize = theight*.66;
	var field = $('#categoryname')
//	Sets new font size
	field.css('font-size',fsize+'px');
	field.css('width', '60%');
//	Make sure font is not bigger than menunav
	fixfontsize();
	
// Find and set new margin
	var fheight = field.height(); 
	var tmargin = ((theight-fheight)*.5);
	field.css('margin-top', tmargin);

}

function fixfontsize(){
	var theight = $('#menunav').height(); 
	var field = $('#categoryname');
	var cheight = field.height();
//	As long as the menubar is shorter than the title...
	while (theight < cheight){
		var fsize = field.css('font-size');
		fsize = parseFloat(fsize)-2;
//	set width to 100% but 20% is menubtn
		field.css('width','75%');
//	Sets new font size
		field.css('font-size',fsize+'px');
//Re-check height before testing again
		cheight = field.height();
		theight = $('#menunav').height();
	}
}

//Run if orentation changes
//shows or hides nav bars and sets mainpage to fill screen
function ortchn(ori){
	var menu = $('#menuitems');
	if (ori == 'portrait'){
		$('#menunav').css('display', 'block');
		menu.css('top', '10%');
		menu.css('padding-bottom', '20%');
		menutitle();
	}else if (ori == 'landscape'){
		$('#menunav').css('display', 'none');
		menu.css('top', '0');
		menu.css('padding-bottom', '0');
	}
}
