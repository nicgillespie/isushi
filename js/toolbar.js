$(document).ready(function(){
	//Run functions on window ready
	addicons();
	fontsize();
	$(window).resize(function(){
//	Run if orentation of device changes
	if (window.orientation == 0){
		var ori = 'portrait';
	}else{
		var ori = 'landscape';
	}
	orichange(ori);
		$(window).bind("orientationchange", function(event){
			orichange(event.orientation);
			fontsize();
		});
	});
});

function addicons(){
//	Array of the #id's of the links 
	var links = new Array('home','info','menu','order');
//	foreach lop of the links adding a div tag inside of teach with the #id as a .class	
	for(var i in links){
		$('#'+links[i]).prepend('<div class='+links[i]+'></div>');
	}
}

function fontsize(){
//	get li height and width and the screen height and width
	var navbar = $('#navbar li');
	var tdocument = $(document);
	var theight = tdocument.height();
	var twidth = tdocument.width();
	var itemheight = navbar.height();
	var itemwidth = navbar.width();
	var textsize;
//	find out if height or width is bigger and set font size
	if (theight > twidth){
		textsize = itemheight/4;
	}else if (theight < twidth){
		textsize = itemwidth/14;
	}
//	apply new font size
	navbar.css('font-size',textsize+'px');

}

//Run if orentation changes
//shows or hides nav bars and sets mainpage to fill screen
function orichange(ori){
	if (ori == 'portrait'){
		$('#navelement').css('display', 'block');
		fontsize();
	}else if (ori == 'landscape'){
		$('#navelement').css('display', 'none');
	}
}
