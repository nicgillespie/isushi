<?php include('./inc/header.php'); ?>
<script>$('body').css('overflow', 'auto');</script>
<script type='text/javascript' src='./js/info.js'></script>

	<!-- ************************************************
	BODY OF PAGE STARTS**********************************
	**************************************************-->
	<div id='ourinfobody'>
		<div id='left'>
			<div id='contactinfo'>
				<h3>Phone Number: 317.272.4263</h3>
				<h3>Have any comments, questions or concerns? Let us know!</h3>
			</div>
			<div id='googlemaps'>
				<h3>Find Us:</h3>
				<h3 id='address'>
				7900 East US highway 36
				Avon, IN 46123
				</h3>
				<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="border-radius: 20px;" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Isushi+Express,+7900+East+US+highway+36,+Avon,+IN+46123-7871&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=42.03917,73.828125&amp;vpsrc=0&amp;ie=UTF8&amp;hq=Isushi+Express,&amp;hnear=7900+E+US+highway+36,+Avon,+Indiana+46123&amp;t=m&amp;cid=12414008234024028886&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Isushi+Express,+7900+East+US+highway+36,+Avon,+IN+46123-7871&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=42.03917,73.828125&amp;vpsrc=0&amp;ie=UTF8&amp;hq=Isushi+Express,&amp;hnear=7900+E+US+highway+36,+Avon,+Indiana+46123&amp;t=m&amp;cid=12414008234024028886&amp;z=14&amp;iwloc=A" style="text-align:left">View on Map</a></small>
			</div>
		</div>
		<div id='right'>
			<div id='contactfourm'>
				<h3>Contact Us</h3>
				<p id='error' style='font-size:65%;'>*-required fields</p>
				<input id='name' type='text' value='Name*' onFocus='clearfield("name");' onBlur='resetfield("name");'>
				<input id='number' type='tel' value='Phone Number' onFocus='clearfield("number");' onBlur='resetfield("number");'>
				<input id='email' type='email' value='Email*' onFocus='clearfield("email");' onBlur='resetfield("email");'>
				<textarea rows='6' id='comment' onFocus='clearfield("comment");' onBlur='resetfield("comment");' >Comment, Question, or Concerns*</textarea>
				<button id='submit' onKeyUp='onKeyDownSubmit(event);' onClick='formSubmit();'>Tell Us</button>
				<p id='results'></p>
			</div>
		</div>
	</div>
</body>
</html>
